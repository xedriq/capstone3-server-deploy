// backend server
// declare and use the dependencies

// use/import the express library
const express = require("express")

// instantiate an express project
// serve our project using express
const app = express();

// use the mongoose library
const mongoose = require("mongoose");

const databaseUrl = process.env.DATABASE_URL || 
    "mongodb+srv://xedriq:xedriq@cluster0-saupb.mongodb.net/pawtastic_db?retryWrites=true&w=majority"

// database connection
mongoose.connect(
    databaseUrl,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false
    }
)

mongoose.connection.once("open", () => {
    console.log("Connected to MongoDB server...")
})


//  import the instantiation of the apollo server
const server = require("./queries/queries.js");


// the app will be served by the apollo server instead 
// of express
server.applyMiddleware({
    app,
    path: "/pawtastic"
})

let port = 4000 || process.env.PORT

// server initialization
app.listen(port, () => {
    console.log(`🚀 https://xedriq-pawtastic.herokuapp.com${server.graphqlPath}`);
})

