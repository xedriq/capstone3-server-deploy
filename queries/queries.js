const { ApolloServer, gql } = require("apollo-server-express");
const { GraphQLDateTime } = require('graphql-iso-date')

//bcrypt js
const bcrypt = require('bcryptjs')

// custom scalar
const customScalarResolver = {
    Date: GraphQLDateTime
}


// models
const User = require('../models/User')
const Pet = require('../models/Pet')
const Service = require('../models/Service')
const Vet = require('../models/Vet')

const typeDefs = gql`

    scalar Date

    type UserType {
        id: ID!
        first_name: String
        last_name: String
        phone: String
        alt_phone: String
        street_address: String
        city: String
        email: String
        password: String
        user_type: String
        pets: [PetType]
        services: [ServiceType]
    }

    type PetType{
        id: ID!
        name: String
        pet_type: String!
        birthday: Date
        breed: String
        photo: String
        gender: String
        spayed_neutered: Boolean
        weight: Int
        favorites: [String]
        food: String
        others: String
        vet_id: String
        vet: VetType
        owner_id: String!
        owner: UserType
        service_id: String
        services: [ServiceType]
    }

    type ServiceType {
        id: String!
        name: String
        pet: PetType
        recurring: Boolean
        start_date: Date
        days: [String]
        times: [String]
        notes: String
        sitter_id: String
        sitter: UserType
        pet_id: String!
        isCompleted: Boolean
    }

    type VetType {
        id: ID!
        first_name: String
        last_name: String
        phone: Int
        street_address: String
        city: String
    }

    type Query {
        getUsers:[UserType]
        getUser(id:String!):UserType
        getUserByEmail(email:String): UserType
        getPets:[PetType]
        getPet(id:String!):PetType
        getServices:[ServiceType]
        getService(id:String!):ServiceType
        getVets:[VetType]
        getVet(id:String!):VetType
    }

    type Mutation {
        createUser(
            first_name: String
            last_name: String
            phone: String
            alt_phone: String
            street_address: String
            city: String
            email: String!
            password: String!
            user_type: String
        ) : UserType

        createPet(
            name: String
            owner_id: String!
            pet_type: String!
            breed: String
            photo: String
            gender: String
            spayed_neutered: Boolean
            weight: Int
            favorites: [String]
            vet_id: String
            food: String
            others: String
            service_id: String
        ) : PetType

        createService(
            #can be walk, drop_in_visit, house_sitting
            name: String
            recurring: Boolean
            start_date: Date
            days: [String]
            times: [String]
            notes: String
            sitter_id: String
            pet_id: String!
            isCompleted: Boolean
        ) : ServiceType

        createVet(
            first_name: String
            last_name: String
            phone: Int
            street_address: String
            city: String
        ) : VetType
        
        updateUser(
            id:String!
            first_name: String
            last_name: String
            phone: String
            alt_phone: String
            street_address: String
            city: String
            password: String
            #user_type: String
            email: String
        ) : UserType

        updatePet(
            id:String
            name: String
            pet_type: String
            birthday: Date
            breed: String
            photo: String
            gender: String
            spayed_neutered: Boolean
            weight: Int
            favorites: [String]
            vet_id: String
            food: String
            others: String
            service_id: String
        ) :PetType

        updateService(
            id:String!
            name: String
            recurring: Boolean
            start_date: String
            days: [String]
            times: [String]
            notes: String
            sitter_id: String
            pet_id: String
            isCompleted: Boolean
        ): ServiceType

        updateVet(
            id:ID!
            first_name: String
            last_name: String
            phone: Int
            street_address: String
            city: String
        ): VetType

        deleteUser(id:String!): Boolean
        deletePet(id:ID!): Boolean
        deleteService(id:String!): Boolean
        deleteVet(id:ID!): Boolean

        logInUser(email:String! password:String!): UserType
        
    }

`

const resolvers = {
    Query: {
        getUsers: () => {
            return User.find({})
        },
        getUser: (parent, args, context, info) => {
            return User.findById(args.id);
        },
        getUserByEmail: (_, args) => {
            return User.findOne({ email: args.email })
        },
        getPets: () => {
            return Pet.find({})
        },
        getPet: (parent, args, context, info) => {
            return Pet.findById(args.id);
        },
        getServices: () => {
            return Service.find({})
        },
        getService: (parent, args, context, info) => {
            return Service.findById(args.id);
        },
        getVets: () => {
            return Vet.find({})
        },
        getVet: (parent, args, context, info) => {
            return Vet.findById(args.id)
        }

    },

    Mutation: {
        createUser: (parent, args, context, info) => {
            let { first_name,
                last_name,
                phone,
                alt_phone,
                street_address,
                city,
                email,
                password,
                pet_ids,
                pets,
                user_type } = args

            let newUser = User({
                first_name,
                last_name,
                phone,
                alt_phone,
                street_address,
                city,
                email,
                password: bcrypt.hashSync(password, 10),
                pet_ids,
                pets,
                user_type: "user"
            })

            return newUser.save()
        },

        createPet: (parent, args, context, info) => {
            let {
                name,
                owner_id,
                owner,
                pet_type,
                breed,
                photo,
                gender,
                spayed_neutered,
                weight,
                favorites,
                vet_id,
                vet,
                food,
                others,
                service_id
            } = args

            let newPet = Pet({
                name,
                owner_id,
                owner,
                pet_type,
                breed,
                photo,
                gender,
                spayed_neutered,
                weight,
                favorites,
                vet_id,
                vet,
                food,
                others,
                service_id
            })

            return newPet.save()
        },

        createService: (parent, args, context, info) => {
            let {
                name,
                recurring,
                start_date,
                days,
                times,
                notes,
                sitter_id,
                sitter,
                pet_id,
                isCompleted
            } = args

            let newService = Service({
                name,
                recurring: false,
                start_date,
                days,
                times,
                notes,
                sitter_id,
                sitter,
                pet_id,
                isCompleted: false
            })

            return newService.save()
        },

        createVet: (parent, args, context, info) => {
            let {
                first_name,
                last_name,
                phone,
                street_address,
                city
            } = args

            let newVet = Vet({
                first_name,
                last_name,
                phone,
                street_address,
                city
            })

            return newVet.save()
        },

        updateUser: (parent, args, context, info) => {
            let { id,
                first_name,
                last_name,
                phone,
                alt_phone,
                street_address,
                city,
                email,
                password,
                pet_ids,
                pets } = args

            let updates = {
                first_name,
                last_name,
                phone,
                alt_phone,
                street_address,
                city,
                email,
                password,
                pet_ids,
                pets,
            }

            let condition = id

            return User.findByIdAndUpdate(condition, updates, { overwrite: false })
        },

        updatePet: (parent, args, context, info) => {
            let {
                id,
                name,
                owner_id,
                owner,
                pet_type,
                breed,
                birthday,
                photo,
                gender,
                spayed_neutered,
                weight,
                favorites,
                vet_id,
                vet,
                food,
                others,
                service_id
            } = args

            let condition = id

            let updates = {
                name,
                owner,
                pet_type,
                breed,
                birthday,
                photo,
                gender,
                spayed_neutered,
                weight,
                favorites,
                vet_id,
                vet,
                food,
                others,
                service_id
            }

            return Pet.findByIdAndUpdate(condition, { $set: updates })
        },

        updateService: (parent, args, context, info) => {
            let {
                id,
                name,
                recurring,
                start_date,
                days,
                times,
                notes,
                sitter_id,
                sitter,
                pet_id,
                isCompleted
            } = args

            let condition = id

            let updates = {
                name,
                recurring,
                start_date,
                days,
                times,
                notes,
                sitter_id,
                sitter,
                pet_id,
                isCompleted
            }

            return Service.findByIdAndUpdate(condition, { $set: updates })
        },

        updateVet: (parent, args, context, info) => {
            let { id,
                first_name,
                last_name,
                phone,
                street_address,
                city
            } = args
            let condition = id
            let updates = {
                first_name,
                last_name,
                phone,
                street_address,
                city
            }

            return Vet.findByIdAndUpdate(condition, { set: updates })
        },

        deleteUser: (parent, { id }, context, info) => {
            return User.findByIdAndDelete(id).then(res => res ? true : false)
        },
        deletePet: (parent, { id }, context, info) => {
            return Pet.findByIdAndDelete(id).then(res => res ? true : false)
        },
        deleteService: (parent, { id }, context, info) => {
            return Service.findByIdAndDelete(id).then(res => res ? true : false)
        },
        deleteVet: (parent, { id }, context, info) => {
            return Vet.findByIdAndDelete(id).then(res => res ? true : false)
        },

        logInUser: (_, { email, password }) => {

            return User.findOne({ email })
                .then(user => {

                    if (!user) {
                        return new Error('User not found.')
                    }

                    let isPasswordMatch = bcrypt.compareSync(
                        password, user.password
                    )

                    if (!isPasswordMatch) {
                        return new Error('Please check your credentials.')
                    }

                    return user

                }).catch(error => {
                    return error.message
                })
        }

    },

    UserType: {
        services: (parent, args, context, info) => {
            return Service.find({ sitter_id: parent._id })
        },

        pets: (parent, args, context, info) => {
            return Pet.find({ owner_id: parent._id })
        }
    },

    PetType: {
        vet: (parent, args, context, info) => {
            return Vet.findById(parent.vet_id)
        },

        owner: (parent, args, context, info) => {
            return User.findById(parent.owner_id)
        },

        services: (parent, args, context, info) => {
            return Service.find({ pet_id: parent._id })
        }
    },

    ServiceType: {
        sitter: (parent, args, context, info) => {
            return User.findById(parent.sitter_id)
        },
        pet: (parent, args) => {
            return Pet.findById(parent.pet_id)
        }
    }
}

const server = new ApolloServer({
    typeDefs,
    resolvers
});

module.exports = server;
