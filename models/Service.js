const mongoose = require('mongoose')
const Schema = mongoose.Schema

const serviceSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    pet_id: {
        type: String,
        required: true
    },
    pet: {
        type: String,
    },
    recurring: {
        type: Boolean,
        required: true
    },
    start_date: {
        type: mongoose.Schema.Types.Date,
        required: true
    },
    days: {
        type: Array,
        required: true
    },
    times: {
        type: Array,
        required: true
    },
    notes: {
        type: String,
    },
    sitter_id: {
        type: String
    },
    isCompleted: {
        type: Boolean,
        require: true
    }
}, {
    timestamps: true
})

module.exports = mongoose.model('Service', serviceSchema)